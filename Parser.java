import java.util.Queue;
import java.util.LinkedList;

public class Parser {
    private Queue<String> tokens;

    public Parser(Queue<String> tokens) {
        this.tokens = new LinkedList<String>();

        for (String s : tokens) {
            this.tokens.add(s);
        }
    }

    private void validate(String token, String correct, String error) throws CompilerException {
        if (token == null || !token.equals(correct)) {
            // System.out.println("token seen: " + token);
            throw new CompilerException(error, CompilerExceptionType.PARSER);
        }
    }

    private Node<ParseNode> parseProg() throws CompilerException {
        Node<ParseNode> tree = new Node<ParseNode>(new ParseNode(ParseNodeType.PROGRAM, null));

        Node<ParseNode> func = parseFunc();

        tree.addChild(func);

        return tree;
    }    

    private Node<ParseNode> parseFunc() throws CompilerException {
        Node<ParseNode> func;
        Node<ParseNode> stmt;

        String error = "Invalid function declaration";
        String token = tokens.poll();

        validate(token, "int", error);

        token = tokens.poll();
        
        if (token != null) {
            if (token.matches("[A-Za-z]+\\w*")) {
                func = new Node<ParseNode>(new ParseNode(ParseNodeType.FUNCTION, token)); // id of Function
            } else {
                throw new CompilerException(error, CompilerExceptionType.PARSER);
            }
        } else {
            throw new CompilerException(error, CompilerExceptionType.PARSER);
        }

        token = tokens.poll();

        validate(token, "(", error);

        token = tokens.poll();

        validate(token, ")", error);

        token = tokens.poll();

        validate(token, "{", error);

        stmt = parseStmt();

        /*Node<ParseNode> stmt1 = new Node<ParseNode>(new ParseNode(ParseNodeType.STATEMENT, null));
        Node<ParseNode> exprSub= new Node<ParseNode>(new ParseNode(ParseNodeType.EXPRESSION, "50"));
        stmt1.addChild(exprSub);
        func.addChild(stmt1);*/ 

        func.addChild(stmt);
       
        token = tokens.poll();

        validate(token, "}", error);
        
        return func;
    }

    private Node<ParseNode> parseStmt() throws CompilerException {
        Node<ParseNode> expr;

        Node<ParseNode> stmt = new Node<ParseNode>(new ParseNode(ParseNodeType.STATEMENT, null));

        String error = "Invalid statement declaration";
        String token = tokens.poll();

        validate(token, "return", error);

        expr = parseExpr();

        stmt.addChild(expr);
        
        /*Node<ParseNode> expr1= new Node<ParseNode>(new ParseNode(ParseNodeType.EXPRESSION, "30"));
        stmt.addChild(expr1);*/

        token = tokens.poll();

        validate(token, ";", error);

        return stmt;
    }

    private Node<ParseNode> parseUnaryOp(String unaryOpsRaw, Node<ParseNode> unaryOp) throws CompilerException {
        // Unary operations resolve to expression (constants) ultimately..

        // Recursively parsing the unary expression: a unary op may contain 1 or more other unary op, and ultimately an expression (constant)
        ParseNode constant = null;

        String error = "Invalid expression (unary operation) declaration";

        // Base case, and recursion, here we go!
        if (unaryOpsRaw.matches("[0-9]+")) {
            constant = new ParseNode(ParseNodeType.EXPRESSION, unaryOpsRaw);
            
            unaryOp.setData(constant); 
        } else if (unaryOpsRaw.matches("[-~!]*[0-9]+")) {
            unaryOp = new Node<ParseNode>(new ParseNode(ParseNodeType.UNARYOPERATION, unaryOpsRaw.substring(0,1)));

            unaryOp.addChild(parseUnaryOp(unaryOpsRaw.substring(1), new Node<ParseNode>()));
        } else {
            // Just check, just in case, though very highly unlikely..
            throw new CompilerException(error, CompilerExceptionType.PARSER);
        }

        return unaryOp;
    }

    private Node<ParseNode> parseExpr() throws CompilerException {
        // An expression is either a unary expression a constant expression
        Node<ParseNode> expr;
        
        Node<ParseNode> constant = null;
        Node<ParseNode> unaryOp = null;

        String error = "Invalid expression declaration";
        String token = tokens.poll();

        if (token != null) {
            if (token.matches("[0-9]+")) {
                // A pure constant token
                expr = new Node<ParseNode>(new ParseNode(ParseNodeType.EXPRESSION, token));
            } else if (token.matches("[-~!]*[0-9]+")) {
                // A unary operation
                expr = new Node<ParseNode>(new ParseNode(ParseNodeType.EXPRESSION, null));
                expr.addChild(parseUnaryOp(token, new Node<ParseNode>())); // parseUnaryOp is recursive..
            } else {
                 throw new CompilerException(error, CompilerExceptionType.PARSER);
            }
        } else {
            throw new CompilerException(error, CompilerExceptionType.PARSER);
        }

        return expr;
    }

    public Node<ParseNode> parse() throws CompilerException {
        return parseProg();
    }
}
