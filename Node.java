import java.util.List;
import java.util.ArrayList;

public class Node<T> {
    private List<Node<T>> children = new ArrayList<Node<T>>();
    private Node<T> parent = null;
    private T data = null;

    public Node(T data) {
        this.data = data;
    }

    public Node() {
    }

    /*public Node(T data, Node<T> parent) {
        this.data = data;
        // this.parent = parent;
        this.setParent(parent);
    }*/

    public List<Node<T>> getChildren() {
        return children;
    }

    // zero-indexed based
    public Node<T> getChild(int i) {
        return children.get(i);
    }

    public int getChildrenCount() {
        return children.size();
    }

    public void setParent(Node<T> parent) {
        this.parent = parent;
    }

    public void addChild(T data) {
        Node<T> newChild = new Node<T>(data);
        this.addChild(newChild);
    }

    public void addChild(Node<T> child) {
        child.setParent(this);
        this.children.add(child);
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isRoot() {
        return (this.parent == null);
    }

    public boolean isLeaf() {
        return this.children.size() == 0;
    }


    public void removeParent() {
        this.parent = null;
    }
}
