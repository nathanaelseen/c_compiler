public class ParseNode {
    private ParseNodeType type;
    private String data;

    public ParseNode(ParseNodeType type, String data) {
        this.type = type;
        this.data = data;
    }

    public ParseNodeType getType() {
        return type;
    }

    public String getData() {
        return data;
    }

    @Override
    public String toString() {
        return type + " " + data;
    } 
}
