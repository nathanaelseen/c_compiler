public class CompilerException extends IllegalArgumentException {
    private String message;
    private CompilerExceptionType type;

    public CompilerException(String message, CompilerExceptionType type) {
        this.type = type;
        this.message = message;
    } 

    @Override
    public String toString() {
        return type + " error: " + message;
    }
}
