enum ParseNodeType {
    PROGRAM("Program"),
    FUNCTION("Function"),
    STATEMENT("Statement"),
    EXPRESSION("Expression"),
    UNARYOPERATION("UnaryOperation"),
    ; 

    private final String type;

    private ParseNodeType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
