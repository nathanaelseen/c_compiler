enum CompilerExceptionType {
    LEXER("Lexer"),
    PARSER("Parser")
    ;

    private final String type;

    private CompilerExceptionType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
