import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
    private StringBuilder rawCode;
    
    private final String regex = "\\{|\\}|\\(|\\)|;|int\\s+|return\\s+|\\b(?!(return|int)\\b)[A-Za-z]+\\w*|[-~!]*[0-9]+";

    // private final String regex = "\\{|\\}|\\(|\\)|;|int\\s+|return\\s+|[A-Za-z]+\\w*|[0-9]+";
    
    public Lexer(StringBuilder rawCode) {
        this.rawCode = new StringBuilder(rawCode.toString());
    }

    private Queue<String> splitWithDelimiters() throws CompilerException {
        String nonDelim, delim;

        List<String> parts = new ArrayList<>();
        Queue<String> result = new LinkedList<>();
        
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(rawCode);
        
        int lastEnd = 0;
        
        while(m.find()) {
            // Start of the match
            int start = m.start();
            
            if(lastEnd != start) {
                nonDelim = rawCode.substring(lastEnd, start);
                parts.add(nonDelim);
            }
            
            delim = m.group();
            parts.add(delim);
            
            int end = m.end();
            lastEnd = end;
        }
        
        if(lastEnd != rawCode.length()) {
            nonDelim = rawCode.substring(lastEnd);
            parts.add(nonDelim);
        }

        for (String s : parts) {
            if (!s.matches("\\s*")) {
                if (s.matches(regex)) {
                    result.add(s.trim());
                } else {
                    // System.out.println("invalid: " + s);
                    throw new CompilerException("Syntax error", CompilerExceptionType.LEXER);
                }
            }
        }
        
        return result;
    }
    public Queue<String> lex() {
        return splitWithDelimiters();
    }
}
