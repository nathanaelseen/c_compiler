public class CodeGenerator {
    private Node<ParseNode> parseTree;

    public CodeGenerator(Node<ParseNode> parseTree) {
        // We need to deep copy the parse tree..
        this.parseTree = copyParseTree(parseTree, new Node<ParseNode>());
        
        // CodeGenerator.printParseTree(this.parseTree, 0);
    }

    public String generateAssembly() {
        return generate(parseTree, new StringBuilder()).toString();
    }

    // This method is used in the constructor to deep copy a parse tree
    private Node<ParseNode> copyParseTree(Node<ParseNode> parseTree, Node<ParseNode> newParseTree) { 
        newParseTree.setData(parseTree.getData());

        int childrenCount = parseTree.getChildrenCount();

        if (childrenCount == 0) {
        } else {
            for (int i = 0; i < childrenCount; ++i) {
                Node<ParseNode> child = parseTree.getChild(i);
                newParseTree.addChild(copyParseTree(child, new Node<ParseNode>()));
            }
        }

        return newParseTree;
    }

    // Traverse the tree to generate x86 assembly
    private StringBuilder generate(Node<ParseNode> parseTree, StringBuilder result) {
        ParseNode curr = parseTree.getData();

        if (curr.getType() == ParseNodeType.FUNCTION) {
            result.append(".globl _" + curr.getData() + "\n");
            result.append("_" + curr.getData() + ":\n");

            // Then start to post order traversal, with child(ren) (remaining)..
            // Note that now is iterative (not recursive anymore)
            int childrenCount = parseTree.getChildrenCount();

            if (childrenCount != 0) {
                for (int i = 0; i < childrenCount; ++i) {
                    Node<ParseNode> child = parseTree.getChild(i);
                    //StringBuilder s = generatePostOrder(child, filePath, result);
                    result.append(generatePostOrder(child, new StringBuilder()));
                    // System.out.println("print" + s);
                    // result.append(s);
                }

                // Basically after this, all child nodes have been accounted for, so we just exit..
                return result;
            }
        } else if (curr.getType() == ParseNodeType.STATEMENT) {
            result.append("ret");
        } else if (curr.getType() == ParseNodeType.EXPRESSION) {
            result.append("movl $" + curr.getData() + ", %eax\n");
        } else if (curr.getType() == ParseNodeType.UNARYOPERATION) {
            switch(curr.getData()) {
                case "-": {
                    result.append("neg %eax\n");
                    break;
                }

                case "~": {
                    result.append("not %eax\n");
                    break;
                }

                case "!": {
                    result.append("cmpl $0, %eax\n");
                        result.append("movl $0, %eax\n");
                    result.append("sete %al\n");
                    break;
                }
            }
        }

        int childrenCount = parseTree.getChildrenCount();
        
        if (childrenCount == 0) {
        } else {
            for (int i = 0; i < childrenCount; ++i) {
                Node<ParseNode> child = parseTree.getChild(i);
                result = generate(child, result);
            }
        }

        return result;
    }

    // Traverse the tree in post order fashion to generate x86 assembly
    private StringBuilder generatePostOrder(Node<ParseNode> parseTree, StringBuilder result) {
        int childrenCount = parseTree.getChildrenCount();

        if (childrenCount == 0) {
        } else {
            for (int i = 0; i < childrenCount; ++i) {
                Node<ParseNode> child = parseTree.getChild(i);
                result = generatePostOrder(child, result);
            }
        }

        ParseNode curr = parseTree.getData();
        
        if (curr.getType() == ParseNodeType.FUNCTION) {
            result.append(".globl _" + curr.getData() + "\n");
            result.append("_" + curr.getData() + ":\n");
        } else if (curr.getType() == ParseNodeType.STATEMENT) {
            result.append("ret");
        } else if (curr.getType() == ParseNodeType.EXPRESSION) {
            // Since unary operation can produce an expression node with null data. 
            if (curr.getData() != null) {
                result.append("movl $" + curr.getData() + ", %eax\n");
            }
        } else if (curr.getType() == ParseNodeType.UNARYOPERATION) {
            switch(curr.getData()) {
                case "-": {
                    result.append("neg %eax\n");
                    break;
                }

                case "~": {
                    result.append("not %eax\n");
                    break;
                }

                case "!": {
                    result.append("cmpl $0, %eax\n");
                    result.append("movl $0, %eax\n");
                    result.append("sete %al\n");
                    break;
                }
            }
        }
 
        return result;

    }

    // Uses recursion to print the parseTree
    public static void printParseTree(Node<ParseNode> parseTree, int indentLevel) {
        if (indentLevel != 0) {
            String tabs = "";
            
            for (int i = 0; i < indentLevel; ++i) {
                tabs += "\t";
            }
            
            System.out.print(tabs);
        }

        System.out.println(parseTree.getData() + ":");
        
        int childrenCount = parseTree.getChildrenCount();

        if (childrenCount == 0) {
        } else {
            for (int i = 0; i < childrenCount; ++i) {
                Node<ParseNode> child = parseTree.getChild(i);
                printParseTree(child, indentLevel + 1);
            }
        }
    }

    // Uses recursion to print the parseTree, using post order traversal
    public static void printParseTreePostOrder(Node<ParseNode> parseTree, int indentLevel) {
        int childrenCount = parseTree.getChildrenCount();

        if (childrenCount == 0) {
        } else {
            for (int i = 0; i < childrenCount; ++i) {
                Node<ParseNode> child = parseTree.getChild(i);
                printParseTreePostOrder(child, indentLevel + 1);
            }
        }

        if (indentLevel != 0) {
            String tabs = "";
            
            for (int i = 0; i < indentLevel; ++i) {
                tabs += "\t";
            }
            
            System.out.print(tabs);
        }

        System.out.println(parseTree.getData() + ":");

    }
}
