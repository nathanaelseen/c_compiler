import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compiler {
    public static void main(String[] args) {
        Parser p;
        Lexer l;
        CodeGenerator cg;
        Node<ParseNode> parseTree;
        Queue<String> tokens;
        String outputASM;

        String outputFilePath;
        String inputFilePath = args[0];

        try {
            StringBuilder rawCode = rawRead(inputFilePath);
            l = new Lexer(rawCode);

            tokens = l.lex(); // Lexing: filter for syntax errors, and raise up invalid tokens

            // No exception, from here, means the tokens are all valid!

            // Quick code for printing out the tokens for debugging
            /*while (tokens.peek() != null) {
                System.out.println(tokens.poll());
            }*/ 
            


            p = new Parser(tokens);

            parseTree = p.parse(); // Parsing: analyse the tokens semantically for their meaning

            // No exception, from here, means the tokens all make sense semantically!

            cg = new CodeGenerator(parseTree);

            outputASM = cg.generateAssembly();
            
            
            /*System.out.println("Printing out generated assembly..");
            System.out.println(outputASM);*/ 

            outputFilePath = constructOutputFilePath(inputFilePath, ".s");
            outputToFile(outputFilePath, outputASM);

            buildExecutableIssueCommand(outputFilePath);

        } catch (Exception e) {
            System.err.println("Error in compilation:");
            System.out.println("\t" + e);
            // e.printStackTrace();
        }
    }

    public static void buildExecutableIssueCommand(String filePath) throws IOException {
        try{
            Runtime rt = Runtime.getRuntime();
            String command = "/usr/local/gcc/8.2.0/bin/gcc " + filePath + " -o " + constructOutputFilePath(filePath, "");
            Process pr = rt.exec(command);
        } catch (IOException e) {
            throw e;
        }
    }

    public static String trimFileExt(String fileName) {
        if (fileName.contains(".")) {
            fileName = fileName.substring(0, fileName.lastIndexOf('.'));
        }

        return fileName;
    }

    public static String constructOutputFilePath(String originalPath, String ext) {
        File f;
        String path;
        String fileName;

        f = new File(originalPath);
        fileName = f.getName();

        fileName = trimFileExt(fileName);

        /*if (fileName.contains(".")) {
            fileName = fileName.substring(0, fileName.lastIndexOf('.'));
        }*/

            
        fileName += ext;
            
        return f.getParent() == null ? fileName : f.getParent() + "/" + fileName; 
    }

    // Outputs the given to File, at the given path
    public static void outputToFile(String filePath, String asm) throws IOException {
        BufferedWriter writer;

        try {
            writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(asm);
            writer.close();
        } catch (IOException e) {
            throw e;
        }
    }

    public static StringBuilder rawRead(String filePath) throws IOException {
        BufferedReader reader;
        StringBuilder sb = new StringBuilder();

        try {
            reader = new BufferedReader(new FileReader(filePath));

            String line = reader.readLine();

            while (line != null) {
                sb.append(line);

                line = reader.readLine();
            }

            reader.close();

            return sb;
        } catch (IOException e) {
            throw e;
        }
    }

}
